from flask import Flask
from flask import request

from pymongo import MongoClient
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
from bson.objectid import ObjectId

# Funcion conexion con Mongo
def database():
    client = MongoClient("localhost", 27017)
    db = client.backend
    return db
app = Flask(__name__)

# Creacion de Alumno
@app.route('/crearAlumno', methods=['GET','POST'])
def crearAlumno():
    db = database()
    alumno = {
        "nombre": request.form['nombre'],
        "ap_materno": request.form['ap_materno'],
        "ap_paterno": request.form['ap_paterno'],
        "direccion": request.form['direccion'],
    }
    alumnos = db.alumnos
    alumnos.insert_one(alumno)
    return('Alumno creado')

# Read - Mostrar todos los Alumnos
@app.route('/mostrarAlumnos',methods = ['POST','GET'])
def mostrarAlumnos():
    db = database()
    alumnos = db.alumnos
    todos = alumnos.find()
    return dumps(todos)

# Update - Editar Alumno
@app.route('/updateAlumno', methods=['GET','POST'])
def updateAlumno():
    db = database()
    id = request.form['id']
    document_id = ObjectId(id)
    alumno = {
        "nombre": request.form['nombre'],
        "ap_materno": request.form['ap_materno'],
        "ap_paterno": request.form['ap_paterno'],
        "direccion": request.form['direccion'],
    }
    query = {'$set': alumno}
    alumnos = db.alumnos
    alumnos.update_one({'_id':document_id},query)
    return('Alumno editado')

# Delete - Eliminar Alumno
@app.route("/deleteAlumno   ",methods = ['POST'])
def eliminar():
    db = database()
    alumnos = db.alumnos
    id = request.form['id']
    document_id = ObjectId(id)
    alumno = alumnos.delete_one({"_id": document_id})
    return ('Alumno eliminado')

# Se agrega la funcion de busqueda, para encontrar el ID asignado al alumno buscado
@app.route("/buscar",methods = ['POST'])
def buscar():
    db = database()
    alumnos = db.alumnos
    id = request.form['id']
    document_id = ObjectId(id)
    alumno = alumnos.find_one({"_id": document_id})
    return dumps(alumno)

# Correr código
if __name__ == '__main__':
    app.run(debug=True)